import balanceReducer from "./balance";
import balanceReducer2 from "./balance";
import * as constants from "../actions/constants";

describe("balanceReducer", () => {
  it("should return the initial state", () => {
    const initialState = 0;
    const expectedResult = initialState;

    expect(balanceReducer(undefined, {})).toEqual(expectedResult);
  });

  describe("when initializing", () => {
    const balance = 10;

    it("should set a balance", () => {
      const action = { type: constants.SET_BALANCE, balance };
      expect(balanceReducer(undefined, action)).toEqual(balance);
    });

    describe("then re-initializing", () => {
      it("should read the balance from cookies", () => {
        expect(balanceReducer2(undefined, {})).toEqual(balance);
      });
    });
  });

  it("should deposit into the balance", () => {
    const deposit = 10;
    const initialState = 5;

    const action = {
      type: constants.SET_DEPOSIT,
      deposit
    };

    expect(balanceReducer(initialState, action)).toEqual(
      initialState + deposit
    );
  });

  it("should withdraw from the balance", () => {
    const initialState = 10;
    const withdrawal = 5;

    const action = {
      type: constants.SET_WITHDRAW,
      withdrawal
    };

    expect(balanceReducer(initialState, action)).toEqual(
      initialState - withdrawal
    );
  });
});
