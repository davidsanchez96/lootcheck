import { FETCH_BITCOIN } from "../actions/constants";

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_BITCOIN:
      return action.bitcoin;
    default:
      return state;
  }
};
