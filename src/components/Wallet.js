import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions/balance";

export class Wallet extends Component {
  state = { balance: 0 };

  updateBalance = event =>
    this.setState({ balance: parseInt(event.target.value, 10) });

  deposit = () => this.props.setDeposit(this.state.balance);

  withdraw = () => this.props.setWithdraw(this.state.balance);

  render() {
    return (
      <div>
        <h3 className="balance">Wallet balance: {this.props.balance}</h3>
        <input className="input-wallet" onChange={this.updateBalance} />
        <button className="btn-deposit" onClick={this.deposit}>
          Deposit
        </button>
        <button className="btn-withdraw" onClick={this.withdraw}>
          Withdraw
        </button>
      </div>
    );
  }
}

const mapStateToProps = ({ balance }) => ({
  balance
});

export default connect(mapStateToProps, actions)(Wallet);
