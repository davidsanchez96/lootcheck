import * as constants from "./constants";

export const setBalance = balance => {
  return {
    type: constants.SET_BALANCE,
    balance
  };
};

export const setDeposit = deposit => {
  return {
    type: constants.SET_DEPOSIT,
    deposit
  };
};

export const setWithdraw = withdrawal => ({
  type: constants.SET_WITHDRAW,
  withdrawal
});
