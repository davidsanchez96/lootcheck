import React, { Component } from "react";
import Wallet from "../components/Wallet";
import Loot from "../components/Loot";

class App extends Component {
  state = {};
  render() {
    return (
      <div>
        <h1>Hello World!</h1>
        <hr />
        <Wallet />
        <hr />
        <Loot />
      </div>
    );
  }
}

export default App;
