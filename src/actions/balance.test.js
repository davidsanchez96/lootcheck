import * as constants from "./constants";
import * as actions from "./balance";

it("should create an action to set the balance", () => {
  const balance = 0;

  const expectedAction = {
    type: constants.SET_BALANCE,
    balance
  };

  expect(actions.setBalance(balance)).toEqual(expectedAction);
});

it("should create an action to deposit into the balance", () => {
  const deposit = 10;

  const expectedAction = {
    type: constants.SET_DEPOSIT,
    deposit
  };

  expect(actions.setDeposit(deposit)).toEqual(expectedAction);
});

it("should create an action to withdraw from the balance", () => {
  const withdrawal = 10;

  const expectedAction = {
    type: constants.SET_WITHDRAW,
    withdrawal
  };

  expect(actions.setWithdraw(withdrawal)).toEqual(expectedAction);
});
