export const SET_BALANCE = "SET_BALANCE";
export const SET_DEPOSIT = "SET_DEPOSIT";
export const SET_WITHDRAW = "SET_WITHDRAW";
export const FETCH_BITCOIN = "FETCH_BITCOIN";
