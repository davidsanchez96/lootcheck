import React from "react";
import { shallow } from "enzyme";
import { Wallet } from "./Wallet";

describe("Wallet", () => {
  const mockDeposit = jest.fn();
  const mockWithdraw = jest.fn();
  const props = {
    balance: 20,
    setDeposit: mockDeposit,
    setWithdraw: mockWithdraw
  };
  const wallet = shallow(<Wallet {...props} />);

  it("should render properly", () => {
    expect(wallet).toMatchSnapshot();
  });

  it("should display the balance from props", () => {
    expect(wallet.find(".balance").text()).toEqual("Wallet balance: 20");
  });

  it("should render an input to deposit into or withdraw from the balance", () => {
    expect(wallet.find(".input-wallet").exists()).toBe(true);
  });

  describe("when the user types into the wallet input", () => {
    const userBalance = "10";

    beforeEach(() => {
      wallet
        .find(".input-wallet")
        .simulate("change", { target: { value: userBalance } });
    });

    it("should update the local wallet balance in `state` and converts it to a number", () => {
      expect(wallet.state().balance).toEqual(parseInt(userBalance, 10));
    });

    describe("and the user wants to make a deposit", () => {
      beforeEach(() => {
        wallet.find(".btn-deposit").simulate("click");
      });

      it("should dispatch the `setDeposit()` it receives from the props with local balance", () => {
        expect(mockDeposit).toHaveBeenCalledWith(parseInt(userBalance, 10));
      });
    });

    describe("and the user wants to make a withdrawal", () => {
      beforeEach(() => {
        wallet.find(".btn-withdraw").simulate("click");
      });

      it("should dispatch the `setWithdraw()` it receives from the props with local balance", () => {
        expect(mockWithdraw).toHaveBeenCalledWith(parseInt(userBalance, 10));
      });
    });
  });
});
